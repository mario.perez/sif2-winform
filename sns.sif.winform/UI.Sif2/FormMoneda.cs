﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;

namespace UI.Sif2
{
    public partial class FormMoneda : FormGenerico
    {
        List<CurrencyData> listaMoneda = new List<CurrencyData>();
        public FormMoneda()
        {
            InitializeComponent();            
        }

        public override void CargarGrid()
        {
            try
            {
                dgv.Rows.Clear();
                listaMoneda = new CatalogsClient().GetAllCurrencies().Data.ToList();
                foreach (var item in listaMoneda)
                {
                    dgv.Rows.Add(item.Id, item.Name, item.CurrencyNumber, item.Decimals
                        , item.Symbol, item.IsActive);
                }

                lblTotalRegistro.Text = "Total de Registro: " + listaMoneda.Count().ToString();
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }


        public override void Agregar()
        {            
            new FormAdminMoneda("I",dgv.Columns,"Agregar Moneda").ShowDialog();
        }

        public override void Consultar()
        {
            new FormAdminMoneda("C", dgv.Columns,"Consultar Moneda").ShowDialog();
        }

        public override void Modificar()
        {
            new FormAdminMoneda("U", dgv.Columns, "Actualizar Moneda").ShowDialog();
        }

        private void FormMoneda_Load(object sender, EventArgs e)
        {
            //Deshabilitamos estos botones porque las moneda servira solo para consultar
            btnNuevo.Visible = btnModificar.Visible = btnConsultar.Visible = btnEliminar.Visible = false;
        }

        public override void RefrescarDatos()
        {
            
            TextBox codigo = (TextBox)this.Controls.Find(Codigo_Filtro_NotNull.Name.ToString(),true)[0];

            if (!string.IsNullOrEmpty(codigo.Text))
            {
                listaMoneda.Clear();
                listaMoneda.Add(new CatalogsClient().GetCurrency(codigo.Text).Data);

                dgv.Rows.Clear();
                foreach (var item in listaMoneda)
                {
                    dgv.Rows.Add(item.Id, item.Name, item.CurrencyNumber, item.Decimals
                        , item.Symbol, item.IsActive);
                }

                lblTotalRegistro.Text = "Total de Registro: " + listaMoneda.Count().ToString();
            }
            else
            {
                CargarGrid();
            }
            
        }
    }
}

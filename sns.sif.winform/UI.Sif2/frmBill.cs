﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;
using serviceLayer.transactionServiceClient.TransactionServiceReference;
using serviceLayer.serviceClient;
using System.ServiceModel;

namespace UI.Sif2
{
    public partial class frmBill : Form
    {
        CatalogsClient _catalogController;
        private int _catalogsLoaded;
        private List<ComboBox> catalogsComboBoxes = new List<ComboBox>();

        public frmBill()
        {
            _catalogController = new CatalogsClient();
            InitializeComponent();
            catalogsComboBoxes.Add(cmbWarehouse);
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void bgwGetWarehouse_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _catalogController.GetAllWarehouses();
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfCataloguenqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwGetWarehouse_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Catalog>();
            Catalogue[] list = new List<Catalogue>().ToArray();
            try
            {
                GeneralMessageOfArrayOfCataloguenqvBR1WM result = (GeneralMessageOfArrayOfCataloguenqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Catalog(list[i]));
                }
                loadWareHouses(catalogs);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            _catalogsLoaded++;
            EnableComboboxes(catalogsComboBoxes.Count == _catalogsLoaded);
        }

        private void loadWareHouses(List<Catalog> catalogs)
        {
            cmbWarehouse.Items.Clear();
            cmbWarehouse.Items.AddRange(catalogs.ToArray());
        }

        private void initializeValues()
        {
            _catalogsLoaded = 0;
            EnableComboboxes(false);
            bgwGetWarehouse.RunWorkerAsync();
            dtpDate.Value = DateTime.Today;
        }


        private void EnableComboboxes(bool v)
        {
            foreach (ComboBox cmb in catalogsComboBoxes)
            {
                cmb.Enabled = v;
            }
        }

        private void frmBill_Load(object sender, EventArgs e)
        {
            initializeValues();
        }

        private void EnableDetail(bool enable)
        {
            lvDetails.Enabled = txtProductId.Enabled = txtAmount.Enabled = cmbPackageId.Enabled = enable;
        }

        private void EnableHeading(bool enable)
        {
            txtId.Enabled = cmbWarehouse.Enabled = dtpDate.Enabled = txtType.Enabled = enable;
        }

        private void Clear()
        {
            txtMessages.Text =  txtId.Text = cmbWarehouse.Text = txtType.Text = string.Empty;
            txtSubTotal.Text = txtDiscount.Text = txtTax.Text = txtTotal.Text = "0.00";
            lblDiscountPercent.Text = @"0%";
            dtpDate.Value = DateTime.Today;
            chkCancelled.Checked = chkRegistered.Checked = false;
            txtProductId.Text = txtDescription.Text = txtMeasurement.Text = txtPrice.Text = 
                txtAmount.Text = cmbPackageId.Text = txtPackageDescripcion.Text = txtQuantity.Text = string.Empty;
            EnableHeading(true);
            EnableDetail(false);
            lvDetails.Items.Clear();
            txtType.Focus();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void txtType_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                EnableDetail(true);
                txtProductId.Focus();
                EnableHeading(false);
            }
        }
    }
}

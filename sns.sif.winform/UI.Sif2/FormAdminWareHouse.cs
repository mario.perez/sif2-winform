﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;

namespace UI.Sif2
{
    public partial class FormAdminWareHouse : FormAdminGenericoCatalogo
    {
        
        public FormAdminWareHouse(Catalogue warehouse, string op, string titulo)
        {
            InitializeComponent();
            catalogue = warehouse;
            operacion = op;
            Text = titulo;
            FillData();
            VerificarTipoOperacion();
        }
        public override void Guardar()
        {

            try
            {
                GeneralMessageOfboolean respuesta = new GeneralMessageOfboolean();
                var category = ActualizarEntidad();
                switch (operacion)
                {
                    case "I":
                        respuesta = new CatalogsClient().AddWarehouse(category);
                        break;
                    case "U":
                        respuesta = new CatalogsClient().UpdateWarehouse(category);
                        break;
                }
                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
    }
}

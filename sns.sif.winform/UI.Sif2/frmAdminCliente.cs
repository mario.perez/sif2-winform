﻿using serviceLayer.serviceClient.CatalogServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.Sif2
{
    public partial class frmAdminCliente : Form
    {
        private ClienteData cliente = null;
        private string operacion = string.Empty;

        public frmAdminCliente(string op, ClienteData c)
        {
            InitializeComponent();
            cliente = c;
            operacion = op;
            NombrarFormulario();
            LlenarFormulario_DatosCliente();
            HabilitarControles_por_TipoOperacion();
        }

        private void HabilitarControles_por_TipoOperacion() {
            if (operacion.Equals("S")) {
                txtCodigo.ReadOnly = true;
                txtCedula.ReadOnly = true;
                txtPrimerNombre.ReadOnly = true;
                txtPrimerApellido.ReadOnly = true;
                txtSegundoNombre.ReadOnly = true;
                txtSegApellido.ReadOnly = true;
                txtNombreFactura.ReadOnly = true;
                txtRuc.ReadOnly = true;
                chkActivo.Enabled = false;
                txtDireccion1.ReadOnly = true;
                txtDireccion2.ReadOnly = true;
                txtDireccion3.ReadOnly = true;
                txtTelefono1.ReadOnly = true;
                txtTelefono2.ReadOnly = true;
                txtEmail1.ReadOnly = true;
                txtEmail2.ReadOnly = true;
                txtFax.ReadOnly = true;
                txtMaximoCredito.ReadOnly = true;
                txtComentario.ReadOnly = true;
                txtFax.ReadOnly = true;
                txtPais.ReadOnly = true;
                txtDepartamento.ReadOnly = true;
                txtCiudad.ReadOnly = true;
                btnGuardar.Enabled = false;
                btnCapturarFoto.Enabled = false;
            }
        }

        private void LlenarFormulario_DatosCliente()
        {
            try
            {
                if (cliente != null) {

                    // Informacion datos personales
                    txtPrimerNombre.Text = cliente.primer_nombre;
                    txtSegundoNombre.Text = cliente.segundo_nombre;
                    txtPrimerApellido.Text = cliente.primer_apellido;
                    txtSegApellido.Text = cliente.segundo_apellido;
                    txtCedula.Text = cliente.Cedula;
                    pbFoto.Image = cliente.Foto != null ? MetodosGenericos.ByteToImage(cliente.Foto) : null;
                    txtRuc.Text = cliente.ruc;
                    chkActivo.Checked = cliente.activo;

                    pbFoto.BackgroundImage = cliente.Foto != null ? null : pbFoto.BackgroundImage;

                    // Informaciòn de Contacto
                    txtDireccion1.Text = cliente.direccion1;
                    txtDireccion2.Text = cliente.direccion2;
                    txtDireccion3.Text = cliente.direccion3;
                    txtTelefono1.Text = cliente.telefono1;
                    txtTelefono2.Text = cliente.telefono2;
                    txtEmail1.Text = cliente.e_mail1;
                    txtEmail2.Text = cliente.e_mail2;                    

                    txtMaximoCredito.Text = cliente.maximo_Credito.ToString();
                    txtComentario.Text = cliente.comentario;
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void NombrarFormulario() {
            switch (operacion)
            {
                case "I":
                    Text = "Agregar Cliente";
                    break;
                case "U":
                    Text = "Modificar Cliente";
                    break;
                case "S":
                    Text = "Consultar Cliente";
                    break;
            }
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            GuardarInformacion();
        }

        private void ActualizarInformacionCliente() {
            try
            {
                if (cliente == null) {
                    cliente = new ClienteData();
                }
                cliente.primer_nombre = txtPrimerNombre.Text;
                cliente.segundo_nombre = txtSegundoNombre.Text;
                cliente.primer_apellido = txtPrimerApellido.Text;
                cliente.segundo_apellido = txtSegApellido.Text;
                cliente.Cedula = txtCedula.Text;
                cliente.codigo_clte = txtCodigo.Text;
                cliente.Foto = (pbFoto.Image != null) ? MetodosGenericos.ImageToByte(pbFoto.Image, System.Drawing.Imaging.ImageFormat.Jpeg) : null;
                cliente.ruc = txtRuc.Text;
                cliente.activo = chkActivo.Checked;

                cliente.direccion1 = txtDireccion1.Text;
                cliente.direccion2 = txtDireccion2.Text;
                cliente.direccion3 = txtDireccion3.Text;
                cliente.telefono1 = txtTelefono1.Text;
                cliente.telefono2 = txtTelefono2.Text;
                cliente.e_mail1 = txtEmail1.Text;
                cliente.e_mail2 = txtEmail2.Text;

                cliente.comentario = txtComentario.Text;
                cliente.maximo_Credito = Convert.ToDecimal(txtMaximoCredito.Text);

                cliente.FechaModificacion = DateTime.Now;

                if (operacion.Equals("I")) {
                    cliente.FechaCreacion = DateTime.Now;
                }

            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void GuardarInformacion() {
            try
            {                
                if (MetodosGenericos.MensajeSINO("¿Está seguro de Guardar los datos del cliente?") == DialogResult.Yes)
                {

                    if (ValidarPrimerNombre() && ValidarPrimerApellido() && ValidarMaximoCredito_Formato()) {
                        ActualizarInformacionCliente();
                        GeneralMessageOfboolean resultado = new GeneralMessageOfboolean();

                        if (operacion.Equals("U"))
                        {
                            resultado = new CatalogsClient().UpdateCliente(cliente);
                        }
                        else
                        {
                            if (operacion.Equals("I"))
                            {
                                resultado = new CatalogsClient().AddCliente(cliente);
                            }
                        }

                        MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(resultado);
                    }
                    else
                    {
                        MetodosGenericos.MensajeAdvertencia("Revisar el ingreso de datos que indica el sistma");
                    }
                    
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void tableLayoutPanel4_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void tableLayoutPanel26_Paint(object sender, PaintEventArgs e)
        {

        }

        private void txtPrimerNombre_Validating(object sender, CancelEventArgs e)
        {
            ValidarPrimerNombre();
        }

        private bool ValidarPrimerNombre() {
            try
            {

                if (string.IsNullOrEmpty(txtPrimerNombre.Text))
                {
                    errorProvider1.SetIconAlignment(txtPrimerNombre, ErrorIconAlignment.MiddleLeft);
                    errorProvider1.SetError(txtPrimerNombre, "Primer Nombre no puede ser vació");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                return false;
            }
        }

        private void txtPrimerApellido_Validating(object sender, CancelEventArgs e)
        {
            ValidarPrimerApellido();
        }

        private bool ValidarPrimerApellido() {
            try
            {
                if (string.IsNullOrEmpty(txtPrimerApellido.Text)) {
                    errorProvider1.SetIconAlignment(txtPrimerApellido, ErrorIconAlignment.MiddleLeft);
                    errorProvider1.SetError(txtPrimerApellido, "Primer Apellido no puede ser vació");
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                return false;
            }
        }

        private void flowLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }        

        private void btnGuardar_Click_1(object sender, EventArgs e)
        {
            GuardarInformacion();
        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CapturarFoto();
        }

        private void CapturarFoto()
        {
            try
            {
                openFileDialog1.Filter = "Image files (*.jpg, *.jpeg) | *.jpg; *.jpeg";

                if (openFileDialog1.ShowDialog() == DialogResult.OK)
                {
                    string imagen = openFileDialog1.FileName;
                    pbFoto.Image = Image.FromFile(imagen);
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void txtMaximoCredito_Validating(object sender, CancelEventArgs e)
        {
            ValidarMaximoCredito_Formato();
        }

        private bool ValidarMaximoCredito_Formato() {
            try
            {                
                decimal maxCredito = Convert.ToDecimal(txtMaximoCredito.Text);
                return true;

            }
            catch (Exception ex)
            {
                errorProvider1.SetError(txtMaximoCredito, ex.Message);
                errorProvider1.SetIconAlignment(txtMaximoCredito, ErrorIconAlignment.MiddleLeft);
                return false;
            }
        }

        private void btnCapturarFoto_Click(object sender, EventArgs e)
        {
            CapturarFoto();
        }

        private void frmAdminCliente_KeyDown(object sender, KeyEventArgs e)
        {
            CombinacionTeclas(e);
        }

        private void CombinacionTeclas(KeyEventArgs e) {
            try
            {
                if (e.KeyCode == Keys.Escape) {
                    Close();
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                throw;
            }
        }
    }
}

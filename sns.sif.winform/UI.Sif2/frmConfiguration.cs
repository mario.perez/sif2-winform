﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;
using serviceLayer.serviceClient;
using System.ServiceModel;

namespace UI.Sif2
{
    public partial class frmConfiguration : Form
    {
        private CatalogsClient _controller;
        private bool _isNew;

        public frmConfiguration()
        {
            _controller = new CatalogsClient();
            _isNew = true;
            InitializeComponent();
        }


        #region Private Methods...

        private void Limpiar()
        {
            txtId.Text = string.Empty;
            txtValue.Text = string.Empty;
            txtDescription.Text = string.Empty;
        }

        private void InitializeValues()
        {
            pbProgress.Visible = true;
            
            EnableAll(false);
            
            Limpiar();
            bgwGetAll.RunWorkerAsync();
            
        }

        private Configs getActualConfiguration()
        {
            var result = new Configs();
            result.Id = txtId.Text;
            result.Value = txtValue.Text;
            result.Description = txtDescription.Text;
            return result;
        }

        private void EnableAll(bool v)
        {
            txtValue.Enabled = v;
            txtDescription.Enabled = v;
            btnSave.Enabled = v;
            btnExit.Enabled = v;
            lstData.Enabled = v;
        }

        private List<Configuration> loadList()
        {
            var catalogs = new List<Configuration>();
            Configs[] list = new List<Configs>().ToArray();
            try
            {
                var result = _controller.GetAllConfigurations();
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;

            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
            for (int i = 0; i < list.Length; i++)
            {
                catalogs.Add(new Configuration(list[i]));
            }
            return catalogs;
        }

        private void loadList(List<Configuration> list)
        {
            lstData.Items.Clear();
            lstData.Items.AddRange(list.ToArray());
        }

        
        private void UpdateOldOne(Configs catalogue)
        {
            try
            {
                var result = _controller.UpdateConfiguration(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }




        #endregion

        #region Events...
        private void frmConfiguration_Load(object sender, EventArgs e)
        {
            InitializeValues();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);
            bgwSave.RunWorkerAsync(getActualConfiguration());
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void lstData_DoubleClick(object sender, EventArgs e)
        {
            if (lstData.SelectedItem != null)
            {
                Configuration selected = (Configuration)lstData.SelectedItem;
                _isNew = false;
                txtId.Text = selected.Id;
                txtValue.Text = selected.Value;
                txtDescription.Text = selected.Description;
            }
        }

        private void frmConfiguration_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !btnExit.Enabled;
        }
        #endregion

        #region Threads Workers...
        private void bgwGetAll_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllConfigurations();
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfConfigsnqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwGetAll_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Configuration>();
            Configs[] list = new List<Configs>().ToArray();
            try
            {
                GeneralMessageOfArrayOfConfigsnqvBR1WM result = (GeneralMessageOfArrayOfConfigsnqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Configuration(list[i]));
                }
                loadList(catalogs);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwSave_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.UpdateConfiguration((Configs)e.Argument);
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfboolean() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwSave_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                var result = (GeneralMessageOfboolean)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                InitializeValues();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }
        
        #endregion
    }
}

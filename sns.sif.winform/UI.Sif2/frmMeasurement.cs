﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;
using serviceLayer.serviceClient;
using System.ServiceModel;

namespace UI.Sif2
{
    public partial class frmMeasurement : Form
    {
        private CatalogsClient _controller;
        private bool _isNew;


        public frmMeasurement()
        {
            _controller = new CatalogsClient();
            _isNew = true;
            InitializeComponent();
        }


        #region Private Methods...

        private void Limpiar()
        {
            txtId.Text = string.Empty;
            txtName.Text = string.Empty;
            
        }

        private void InitializeValues()
        {
            pbProgress.Visible = true;
            EnableAll(false);
            Limpiar();
            bgwGetAll.RunWorkerAsync();

        }

        private void EnableAll(bool v)
        {
            txtName.Enabled = v;
            
            btnNew.Enabled = v;
            btnRemove.Enabled = v;
            btnSave.Enabled = v;
            btnExit.Enabled = v;
            lstData.Enabled = v;
        }

        private List<Catalog> loadList()
        {
            var catalogs = new List<Catalog>();
            Catalogue[] list = new List<Catalogue>().ToArray();
            try
            {
                var result = _controller.GetAllWarehouses();
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;

            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
            for (int i = 0; i < list.Length; i++)
            {
                catalogs.Add(new Catalog(list[i]));
            }
            return catalogs;
        }

        private void loadList(List<Catalog> list)
        {
            lstData.Items.Clear();
            lstData.Items.AddRange(list.ToArray());
        }


        private void RemoveOne(Catalogue catalogue)
        {
            try
            {
                var result = _controller.RemoveWarehouse(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void UpdateOldOne(Catalogue catalogue)
        {
            try
            {
                var result = _controller.UpdateWarehouse(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void AddNew(Catalogue catalogue)
        {
            try
            {
                var result = _controller.AddWarehouse(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }




        }




        #endregion

        #region Events...
        private void frmMeasurement_Load(object sender, EventArgs e)
        {
            InitializeValues();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            
            txtName.Text = string.Empty;
            txtId.Text = string.Empty;
            txtName.Focus();
            _isNew = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);
            bgwRemove.RunWorkerAsync(new Catalogue() { Id = txtId.Text, Value = txtName.Text, IsActive = false });

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);
            bgwSave.RunWorkerAsync(new Catalogue() { Id = txtId.Text, Value = txtName.Text, IsActive = false });
        }

        private void lstData_DoubleClick(object sender, EventArgs e)
        {
            if (lstData.SelectedItem != null)
            {
                Catalog selected = (Catalog)lstData.SelectedItem;
                _isNew = false;
                txtId.Text = selected.Id;
                txtName.Text = selected.Value;
                
            }
        }

        private void frmMeasurement_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !btnExit.Enabled;
        }
        #endregion

        #region Threads Workers...
        private void bgwGetAll_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllMeasurements();
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfCataloguenqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwGetAll_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Catalog>();
            Catalogue[] list = new List<Catalogue>().ToArray();
            try
            {
                GeneralMessageOfArrayOfCataloguenqvBR1WM result = (GeneralMessageOfArrayOfCataloguenqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Catalog(list[i]));
                }
                loadList(catalogs);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwSave_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (_isNew)
                {
                    e.Result = _controller.AddMeasurement((Catalogue)e.Argument);
                }
                else
                {
                    e.Result = _controller.UpdateMeasurement((Catalogue)e.Argument);
                }
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfboolean() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwSave_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                var result = (GeneralMessageOfboolean)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                InitializeValues();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwRemove_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.RemoveMeasurement((Catalogue)e.Argument);
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfboolean() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwRemove_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                var result = (GeneralMessageOfboolean)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                MessageBox.Show("El dato se eliminó correctamente.", "Exitoso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                InitializeValues();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }
        #endregion
    }
}

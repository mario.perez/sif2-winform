﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.Security;
using serviceLayer.Security.SecurityServiceReference;

namespace UI.Sif2
{
    public partial class frmRoleLista : Form
    {
        private SecurityClient _controller;
        private RolData rolFiltro = new RolData();      
        private UsuarioRolData[] ListaRoles = new List<UsuarioRolData>().ToArray();
        int UsuarioId, RolIdPaste;
        private string Llamado_desde = string.Empty;

        public frmRoleLista(int usuId, int rolId, string llamado)
        {
            InitializeComponent();
            _controller = new SecurityClient();
            UsuarioId = usuId;
            RolIdPaste = rolId;
            Llamado_desde = llamado;
        }

        private RolData ObtenerClienteSeleccionado()
        {
            RolData rolSeleccionado = null;

            try
            {
                if (dgvRoles.CurrentRow != null && dgvRoles.CurrentRow.Index > 0)
                {
                    rolSeleccionado = new RolData();
                    var id = Convert.ToInt32(dgvRoles.CurrentRow.Cells["Id"].Value);
                    rolSeleccionado.Id = id;
                    rolSeleccionado.FechaCreacion = DateTime.Now;
                    rolSeleccionado.FechaModificacion = DateTime.Now;
                    
                    rolSeleccionado = _controller.GetAllRol(rolSeleccionado).Data[0];
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
            return rolSeleccionado;
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            //CrearRol();
        }
        private void ConsultarRol()
        {
            try
            {
                var usu = ObtenerClienteSeleccionado();
                if (usu != null) {
                    var form = new frmAdminRol("S", usu);
                    form.Text = "Consultar Rol";
                    form.ShowDialog();
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void EliminarRol()
        {
            try
            {
                var usu = ObtenerClienteSeleccionado();
                if (usu != null) {
                    if (MetodosGenericos.MensajeSINO("¿Está seguro de eliminar el Rol?") == DialogResult.Yes)
                    {
                        var resultado = _controller.RemoveRol(usu);
                        MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(resultado);
                    }
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            //ModificarRol();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarRol();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ConsultarRol();
        }

        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void GetAllRol() {
            try
            {                
                bgw_GetRol.RunWorkerAsync();
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void bgw_GetRol_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllUsuarioRol_NoRol(UsuarioId);
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfRolDatanqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgw_GetRol_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ObtenerRol(e);
        }

        private void ObtenerRol(RunWorkerCompletedEventArgs e) {
            UsuarioRolData[] list = new List<UsuarioRolData>().ToArray();
            try
            {
                GeneralMessageOfArrayOfUsuarioRolDatanqvBR1WM result = (GeneralMessageOfArrayOfUsuarioRolDatanqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);

                list = result.Data;
                ListaRoles = result.Data;                

                LlenarDataGrid(string.Empty, string.Empty);

            }
            catch (ArgumentException ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void LlenarDataGrid(string nombre, string descripcion)
        {

            dgvRoles.CellValueChanged -= dgvRoles_CellValueChanged;
            MetodosGenericos.FormatearDataGridView(dgvRoles);
            dgvRoles.CellValueChanged += dgvRoles_CellValueChanged;            

            foreach (var item in ListaRoles)
            {

                if (
                       (string.IsNullOrEmpty(nombre) || string.IsNullOrWhiteSpace(nombre) || item.NombreRol.Contains(nombre))
                       && (string.IsNullOrEmpty(descripcion) || string.IsNullOrWhiteSpace(descripcion) || item.DescripcionRol.Contains(descripcion))                       
                    )
                {
                    dgvRoles.Rows.Add(
                        item.RolId
                        , item.NombreRol
                        , item.DescripcionRol
                    );

                    // Indicamos que los registros mayores al primero son de lectura
                    dgvRoles.Rows[dgvRoles.Rows.Count - 1].ReadOnly = true;                    
                }

            }
            // Al count le restamos -1 para que no tome el cuenta el registro usado para filtrar el grid
            lbl_totalRegistro.Text = "Total Registros: " + (dgvRoles.Rows.Count - 1).ToString();
        }

        private void dgvRoles_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == 0)
            {
                if (dgvRoles.CurrentRow != null)
                {

                    string Nombre = dgvRoles.CurrentRow.Cells["Nombre"].Value != null ? dgvRoles.CurrentRow.Cells["Nombre"].Value.ToString() : string.Empty;
                    string Descripcion = dgvRoles.CurrentRow.Cells["Descripcion"].Value != null ? dgvRoles.CurrentRow.Cells["Descripcion"].Value.ToString() : string.Empty;                    
                    //bool activo = dgvRoles.CurrentRow.Cells["Activo"].Value != null ? bool.Parse(dgvRoles.CurrentRow.Cells["Activo"].Value.ToString()) : false;

                    LlenarDataGrid(Nombre, Descripcion);
                }
            }
        }

        private void frmRole_Load(object sender, EventArgs e)
        {
            GetAllRol();
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            GetAllRol();
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //CrearRol();
        }

        private void modificartoolStripMenuItem_Click(object sender, EventArgs e)
        {
            //ModificarRol();
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EliminarRol();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarRol();
        }

        private void btnFiltros_Click(object sender, EventArgs e)
        {                        
            //MetodosGenericos.MostrarOcultarPanelFiltro(anchoPanelFiltro,panel1);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            //LimpiarFiltros();   
        }
       

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            GuardarRolSeleccionado();
        }

        private void GuardarRolSeleccionado() {
            try
            {
                if (dgvRoles.CurrentRow != null)
                {
                    if (dgvRoles.CurrentRow.Index > 0)
                    {
                        GeneralMessageOfboolean resultado = new GeneralMessageOfboolean();
                        int rolId = Int32.Parse(dgvRoles.CurrentRow.Cells["Id"].Value.ToString());

                        switch (Llamado_desde)
                        {
                            case "frmAdminRol":
                                resultado = _controller.CopyPrivilegios_FromRol(rolId, RolIdPaste);
                                break;
                            case "frmAdminUsuario":
                                resultado = _controller.AddUsuarioRol(UsuarioId, rolId, 0, "I");
                                break;
                        }

                        
                        MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(resultado);
                    }
                    else
                    {
                        MetodosGenericos.MensajeInformativo("Debe seleccionar un Rol.");
                    }
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }


    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;
using serviceLayer.Security.SecurityServiceReference;

namespace UI.Sif2
{
    public partial class FormGenericoCatalogo : Form
    {

        [Description("Tag del boton nuevo"), Browsable(true)]
        public string TagBtnNuevo
        {
            get
            {
                return btnNuevo.Tag.ToString();
            }
            set
            {
                this.btnNuevo.Tag = value;
            }
        }

        [Description("Tag del boton Modificar"), Browsable(true)]
        public string TagBtnModificar
        {
            get
            {
                return btnModificar.Tag.ToString();
            }
            set
            {
                this.btnModificar.Tag = value;
            }
        }

        [Description("Tag del boton Eliminar"), Browsable(true)]
        public string TagBtnEliminar
        {
            get
            {
                return btnEliminar.Tag.ToString();
            }
            set
            {
                this.btnEliminar.Tag = value;
            }
        }

        [Description("Tag del boton Consultar"), Browsable(true)]
        public string TagBtnConsultar
        {
            get
            {
                return btnConsultar.Tag.ToString();
            }
            set
            {
                this.btnConsultar.Tag = value;
            }
        }

        public FormGenericoCatalogo()
        {
            InitializeComponent();

        }

        public virtual Catalogue ObtenerCatalogue()
        {
            try
            {
                
                if (dgv.CurrentRow != null)
                {
                    Catalogue cat = new Catalogue();
                    cat.Id = dgv.CurrentRow.Cells["Codigo"].Value.ToString();
                    cat.Value = dgv.CurrentRow.Cells["Nombre"].Value.ToString();
                    cat.IsActive = bool.Parse(dgv.CurrentRow.Cells["Activo"].Value.ToString());
                    return cat;
                }
                else
                {
                    return null;
                }

                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                return null;
            }
        }

        public virtual void LoadCatalog(Catalogue[] lista)
        {
            try
            {

                dgv.Rows.Clear();
                foreach (var item in lista)
                {
                    dgv.Rows.Add(
                        item.Id
                        , item.Value
                        , item.IsActive
                        );
                }
                lblTotalRegistro.Text = "Total  Registros: " + dgv.Rows.Count.ToString();
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }


        public virtual void Agregar() { }

        public virtual void Modificar() { }

        public virtual void Consultar() {
            
        }        

        public virtual void Eliminar() { }        

        public virtual void RefrescarDatos() { }

        public virtual void LimpiarFiltro() { }


        public void btnCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        

        public void FormGenerico_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.Escape)
            {
                Close();
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            Agregar();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            Modificar();
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            Eliminar();
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            Consultar();
        }

        private void btnFiltrar_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarSplitFiltro(splitContainer1);
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Agregar();
        }

        private void modificartoolStripMenuItem_Click(object sender, EventArgs e)
        {
            Modificar();
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Eliminar();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Consultar();
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            RefrescarDatos();
        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarFiltro();
        }

        public void HabilitarPermisos()
        {
            try
            {
                var acciones = new SecurityClient().GetPrivilegioAccion(MetodosGenericos.UsuarioConectado.Id, this.Tag.ToString()).Data.ToList();
                MetodosGenericos.Habilitar_Operacion_PrivilegioAccion(barraOperacion, acciones);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;
using serviceLayer.serviceClient;
using System.ServiceModel;

namespace UI.Sif2
{
    public partial class frmProvider : Form
    {
        private CatalogsClient _controller;
        private bool _isNew;

        public frmProvider()
        {
            _controller = new CatalogsClient();
            _isNew = true;
            InitializeComponent();
        }


        #region Private Methods...

        private void Limpiar()
        {
            txtId.Text = string.Empty;
            txtCodeId.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtContact.Text = string.Empty;
            txtName.Text = string.Empty;
            txtTelephone.Text = string.Empty;
            

        }

        private ProviderData getActualData()
        {
            var result = new ProviderData();
            result.ProviderId = txtId.Text == "" ? "0" : txtId.Text;
            result.ProviderName = txtName.Text;
            result.IsActive = false;
            result.Phone = txtTelephone.Text;
            result.ProviderCodeId = txtCodeId.Text;
            result.Address = txtAddress.Text;
            result.Email = txtEmail.Text;
            result.Contact = txtContact.Text;
            
            return result;
        }

        private void InitializeValues()
        {
            pbProgress.Visible = true;
            EnableAll(false);
            Limpiar();
            bgwGetAll.RunWorkerAsync();
        }

        private void EnableAll(bool v)
        {
            txtName.Enabled = v;
            
            txtTelephone.Enabled = v;
            txtEmail.Enabled = v;
            txtContact.Enabled = v;
            txtAddress.Enabled = v;
            txtCodeId.Enabled = v;

            btnNew.Enabled = v;
            btnRemove.Enabled = v;
            btnSave.Enabled = v;
            btnExit.Enabled = v;
            lstData.Enabled = v;
        }

        private List<Provider> loadList()
        {
            var catalogs = new List<Provider>();
            ProviderData[] list = new List<ProviderData>().ToArray();
            try
            {
                var result = _controller.GetAllProviders();
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;

            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
            for (int i = 0; i < list.Length; i++)
            {
                catalogs.Add(new Provider(list[i]));
            }
            return catalogs;
        }

        private void loadList(List<Provider> list)
        {
            lstData.Items.Clear();
            lstData.Items.AddRange(list.ToArray());
        }


        private void RemoveOne(ProviderData catalogue)
        {
            try
            {
                var result = _controller.RemoveProvider(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void UpdateOldOne(ProviderData catalogue)
        {
            try
            {
                var result = _controller.UpdateProvider(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
        }

        private void AddNew(ProviderData catalogue)
        {
            try
            {
                var result = _controller.AddProvider(catalogue);
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
            }
            catch (FaultException ex)
            {
                throw new ArgumentException(ex.Message);
            }
            
        }




        #endregion

        #region Events...
        private void frmProvider_Load(object sender, EventArgs e)
        {
            InitializeValues();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            Limpiar();
            txtCodeId.Focus();
            _isNew = true;
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);
            bgwRemove.RunWorkerAsync(getActualData());
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            pbProgress.Visible = true;
            EnableAll(false);
            bgwSave.RunWorkerAsync(getActualData());
        }

        private void lstData_DoubleClick(object sender, EventArgs e)
        {
            if (lstData.SelectedItem != null)
            {
                Provider selected = (Provider)lstData.SelectedItem;
                _isNew = false;
                txtId.Text = selected.ProviderId.ToString();
                txtAddress.Text = selected.Address;
                txtCodeId.Text = selected.ProviderCodeId;
                txtContact.Text = selected.Contact;
                txtEmail.Text = selected.Email;
                txtName.Text = selected.ProviderName;
                txtTelephone.Text = selected.Phone;
                
                
            }
        }

        private void frmProvider_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = !btnExit.Enabled;
        }


        #endregion

        #region Threads Workers...
        private void bgwGetAll_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllProviders();
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfProviderDatanqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwGetAll_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            var catalogs = new List<Provider>();
            ProviderData[] list = new List<ProviderData>().ToArray();
            try
            {
                GeneralMessageOfArrayOfProviderDatanqvBR1WM result = (GeneralMessageOfArrayOfProviderDatanqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                list = result.Data;
                for (int i = 0; i < list.Length; i++)
                {
                    catalogs.Add(new Provider(list[i]));
                }
                loadList(catalogs);
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwSave_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (_isNew)
                {
                    e.Result = _controller.AddProvider((ProviderData)e.Argument);
                }
                else
                {
                    e.Result = _controller.UpdateProvider((ProviderData)e.Argument);
                }
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfboolean() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwSave_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                var result = (GeneralMessageOfboolean)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                InitializeValues();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }

        private void bgwRemove_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.RemoveProvider((ProviderData)e.Argument);
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfboolean() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwRemove_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                var result = (GeneralMessageOfboolean)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);
                MessageBox.Show("El dato se eliminó correctamente.", "Exitoso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                InitializeValues();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            EnableAll(true);
            pbProgress.Visible = false;
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;

namespace UI.Sif2
{
    public partial class FormAdminGenericoCatalogo : Form
    {

        [Description("Activar lectura o escritura TxtCodigo"), Browsable(true)]
        public bool ReadOnlyTxtCodigo
        {
            get
            {
                return txtCodigo.ReadOnly;
            }
            set
            {
                this.txtCodigo.ReadOnly = value;
            }
        }

        public Catalogue catalogue;
        public string operacion;


        public FormAdminGenericoCatalogo(Catalogue cat, string op)
        {
            InitializeComponent();            
        }              

        public Catalogue ActualizarEntidad()
        {
            try
            {
                var cat = new Catalogue();
                cat.Id = txtCodigo.Text;
                cat.Value = txtNombre.Text;
                cat.IsActive = chkActivo.Checked;
                return cat;
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
                return null;
            }            
        }

        public FormAdminGenericoCatalogo()
        {
            InitializeComponent();
        }

        public virtual void FillData()
        {
            try
            {
                if (!operacion.Equals("I"))
                {
                    txtNombre.Text = catalogue.Value;
                    txtCodigo.Text = catalogue.Id;
                    chkActivo.Checked = catalogue.IsActive;
                }
                else
                {
                    chkActivo.Checked = true;
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        public virtual void VerificarTipoOperacion()
        {
            if (operacion.Equals("C"))
            {
                ActivarLecturaControles();
            }
        }

        public virtual void ActivarLecturaControles()
        {
            this.ObtenerControles<TextBox>().ForEach(x => x.ReadOnly = true);
            this.ObtenerControles<Button>().ForEach(x => x.Enabled = false);
            this.ObtenerControles<CheckBox>().ForEach(x => x.Enabled = false);
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            Guardar();            
        }

        public virtual void Guardar() { }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void FormAdminGenericoCatalogo_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keys.G == e.KeyData && Keys.Control == e.KeyData)
            {
                Guardar();
            }

            if (e.KeyData == Keys.Escape)
            {
                Close();
            }
        }

        private void btnGuardar_Click_1(object sender, EventArgs e)
        {
            Guardar();
        }

        private void toolStripButton2_Click_1(object sender, EventArgs e)
        {
            Close();
        }
    }
}

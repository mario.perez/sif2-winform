﻿namespace UI.Sif2
{
    partial class FormProveedor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dgv = new System.Windows.Forms.DataGridView();
            this.Codigo_Filtro_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Identificacion_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NombreProveedor_NotNull = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.EsActivo_NotNull = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Telefono = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Contacto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Correo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Direccion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Size = new System.Drawing.Size(677, 259);
            this.splitContainer1.SplitterDistance = 181;
            // 
            // panel1
            // 
            this.panel1.Size = new System.Drawing.Size(488, 22);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.dgv);
            this.panel2.Size = new System.Drawing.Size(488, 235);
            // 
            // panelFiltro
            // 
            this.panelFiltro.Size = new System.Drawing.Size(179, 235);
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.InactiveCaption;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.BackgroundColor = System.Drawing.Color.White;
            this.dgv.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.ColumnHeadersHeight = 30;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Codigo_Filtro_NotNull,
            this.Identificacion_NotNull,
            this.NombreProveedor_NotNull,
            this.EsActivo_NotNull,
            this.Telefono,
            this.Contacto,
            this.Correo,
            this.Direccion});
            this.dgv.ContextMenuStrip = this.MenuOperacion;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 0);
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            this.dgv.RowHeadersVisible = false;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(488, 235);
            this.dgv.TabIndex = 25;
            // 
            // Codigo_Filtro_NotNull
            // 
            this.Codigo_Filtro_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Codigo_Filtro_NotNull.HeaderText = "Codigo";
            this.Codigo_Filtro_NotNull.Name = "Codigo_Filtro_NotNull";
            this.Codigo_Filtro_NotNull.ReadOnly = true;
            this.Codigo_Filtro_NotNull.Width = 83;
            // 
            // Identificacion_NotNull
            // 
            this.Identificacion_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Identificacion_NotNull.HeaderText = "Identificación";
            this.Identificacion_NotNull.MaxInputLength = 32;
            this.Identificacion_NotNull.Name = "Identificacion_NotNull";
            this.Identificacion_NotNull.ReadOnly = true;
            this.Identificacion_NotNull.Width = 125;
            // 
            // NombreProveedor_NotNull
            // 
            this.NombreProveedor_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.NombreProveedor_NotNull.HeaderText = "Nombre Proveedor";
            this.NombreProveedor_NotNull.MaxInputLength = 128;
            this.NombreProveedor_NotNull.Name = "NombreProveedor_NotNull";
            this.NombreProveedor_NotNull.ReadOnly = true;
            this.NombreProveedor_NotNull.Width = 165;
            // 
            // EsActivo_NotNull
            // 
            this.EsActivo_NotNull.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.EsActivo_NotNull.HeaderText = "Es Activo";
            this.EsActivo_NotNull.Name = "EsActivo_NotNull";
            this.EsActivo_NotNull.ReadOnly = true;
            this.EsActivo_NotNull.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.EsActivo_NotNull.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.EsActivo_NotNull.Width = 98;
            // 
            // Telefono
            // 
            this.Telefono.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Telefono.HeaderText = "Telefono";
            this.Telefono.MaxInputLength = 32;
            this.Telefono.Name = "Telefono";
            this.Telefono.ReadOnly = true;
            this.Telefono.Width = 95;
            // 
            // Contacto
            // 
            this.Contacto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Contacto.HeaderText = "Contacto";
            this.Contacto.MaxInputLength = 256;
            this.Contacto.Name = "Contacto";
            this.Contacto.ReadOnly = true;
            this.Contacto.Width = 94;
            // 
            // Correo
            // 
            this.Correo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.Correo.HeaderText = "Correo Eléctronico";
            this.Correo.MaxInputLength = 128;
            this.Correo.Name = "Correo";
            this.Correo.ReadOnly = true;
            this.Correo.Width = 162;
            // 
            // Direccion
            // 
            this.Direccion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Direccion.HeaderText = "Dirección";
            this.Direccion.Name = "Direccion";
            this.Direccion.ReadOnly = true;
            // 
            // FormProveedor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(677, 284);
            this.Name = "FormProveedor";
            this.Tag = "PROVE_CONTACT";
            this.TagBtnConsultar = "GETPROVE";
            this.TagBtnEliminar = "REMOVEPROVE";
            this.TagBtnModificar = "UPDATEPROVE";
            this.TagBtnNuevo = "ADDPROVE";
            this.Text = "Mantenimiento de Proveedor";
            this.Load += new System.EventHandler(this.FormProveedor_Load);
            this.Controls.SetChildIndex(this.splitContainer1, 0);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn Codigo_Filtro_NotNull;
        private System.Windows.Forms.DataGridViewTextBoxColumn Identificacion_NotNull;
        private System.Windows.Forms.DataGridViewTextBoxColumn NombreProveedor_NotNull;
        private System.Windows.Forms.DataGridViewCheckBoxColumn EsActivo_NotNull;
        private System.Windows.Forms.DataGridViewTextBoxColumn Telefono;
        private System.Windows.Forms.DataGridViewTextBoxColumn Contacto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Correo;
        private System.Windows.Forms.DataGridViewTextBoxColumn Direccion;
    }
}
﻿using serviceLayer.serviceClient.CatalogServiceReference;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UI.Sif2
{
    public partial class FormAdminSalesMan : FormAdminGenericoCatalogo
    {
        public FormAdminSalesMan(Catalogue cat, string op, string titulo)
        {
            InitializeComponent();
            catalogue = cat;
            operacion = op;
            Text = titulo;
            FillData();
            VerificarTipoOperacion();
        }

        public override void Guardar()
        {

            try
            {
                GeneralMessageOfboolean respuesta = new GeneralMessageOfboolean();
                var vendedor = ActualizarEntidad();
                switch (operacion)
                {
                    case "I":
                        respuesta = new CatalogsClient().AddSalesman(vendedor);
                        break;
                    case "U":
                        respuesta = new CatalogsClient().UpdateSalesman(vendedor);
                        break;
                }
                MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(respuesta);
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
    }
}

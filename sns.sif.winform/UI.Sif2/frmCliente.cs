﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Forms;
using serviceLayer.serviceClient.CatalogServiceReference;
using serviceLayer.serviceClient;
using System.ServiceModel;
using System.Collections.ObjectModel;
using System.Drawing;

namespace UI.Sif2
{
    public partial class frmCliente : Form
    {
        private CatalogsClient _controller;        
        private Cliente clienteFiltro = new Cliente(new ClienteData());
        private ClienteData[] listaCliente = new List<ClienteData>().ToArray();
        private int anchoPanelFiltro = 349;
        public frmCliente()
        {
            InitializeComponent();
            _controller = new CatalogsClient();            
            panel1.Width = anchoPanelFiltro;
        }

        private void bgwGetAllClientes_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                e.Result = _controller.GetAllCliente(0);
            }
            catch (Exception ex)
            {
                e.Result = new GeneralMessageOfArrayOfClienteDatanqvBR1WM() { IsError = true, CodeResponse = -1, MessageResponse = ex.Message };
            }
        }

        private void bgwGetAllClientes_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ListarClientes_al_Grid(e);
        }

        private void ListarClientes_al_Grid(RunWorkerCompletedEventArgs e) {
            ClienteData[] list = new List<ClienteData>().ToArray();
            try
            {
                GeneralMessageOfArrayOfClienteDatanqvBR1WM result = (GeneralMessageOfArrayOfClienteDatanqvBR1WM)e.Result;
                if (result.IsError)
                    throw new ArgumentException(result.MessageResponse);

                list = result.Data;
                listaCliente = result.Data;
                
                //lbl_totalRegistro.Text = "Total Registro: " + listaCliente.Length.ToString(); 

                LlenarDataGrid(string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                    string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty,
                    string.Empty, true
                    );
                
            }
            catch (ArgumentException ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void LlenarDataGrid(string nombreCompleto
            , string pais, string departamento, string ciudad
            , string direccion1, string direccion2, string direccion3
            , string correo1, string correo2
            , string telefono1, string telefono2, string fax
            , string ruc, string cedula, string comentario, string maximoCredito            
            ,bool activo
            ) {

            try
            {

                //dgvClientes.CellValueChanged -= dgvClientes_CellValueChanged;
                //MetodosGenericos.FormatearDataGridView(dgvClientes);
                //dgvClientes.CellValueChanged += dgvClientes_CellValueChanged;
                //int cont = 0; // variable contara la cant de registros insertado en el datagrid
                dgvClientes.Rows.Clear();

                foreach (var item in listaCliente)
                {

                    string nombreCompletoData = ((!string.IsNullOrEmpty(item.primer_nombre) ? item.primer_nombre : string.Empty)
                            + " " + (!string.IsNullOrEmpty(item.segundo_nombre) ? item.segundo_nombre : string.Empty)
                            + " " + (!string.IsNullOrEmpty(item.primer_apellido) ? item.primer_apellido : string.Empty)
                            + " " + (!string.IsNullOrEmpty(item.segundo_apellido) ? item.segundo_apellido : string.Empty)).Trim();

                    decimal maxcred = string.IsNullOrEmpty(maximoCredito) || string.IsNullOrWhiteSpace(maximoCredito) ? 0 : decimal.Parse(maximoCredito);

                    if (
                            (nombreCompletoData.Contains(nombreCompleto) || string.IsNullOrEmpty(nombreCompleto) || string.IsNullOrWhiteSpace(nombreCompleto))
                            && (item.Cedula.Contains(cedula) || string.IsNullOrEmpty(cedula) || string.IsNullOrWhiteSpace(cedula))

                            && (item.direccion1.Contains(direccion1) || string.IsNullOrEmpty(direccion1) || string.IsNullOrWhiteSpace(direccion1))
                            && (item.direccion2.Contains(direccion2) || string.IsNullOrEmpty(direccion2) || string.IsNullOrWhiteSpace(direccion2))
                            && (item.direccion3.Contains(direccion3) || string.IsNullOrEmpty(direccion3) || string.IsNullOrWhiteSpace(direccion3))

                            && (item.telefono1.Contains(telefono1) || string.IsNullOrEmpty(telefono1) || string.IsNullOrWhiteSpace(telefono1))
                            && (item.telefono2.Contains(telefono2) || string.IsNullOrEmpty(telefono2) || string.IsNullOrWhiteSpace(telefono2))

                            && (item.e_mail1.Contains(correo1) || string.IsNullOrEmpty(correo1) || string.IsNullOrWhiteSpace(correo1))
                            && (item.e_mail2.Contains(correo2) || string.IsNullOrEmpty(correo2) || string.IsNullOrWhiteSpace(correo2))

                            && (item.fax.Contains(fax) || string.IsNullOrEmpty(fax) || string.IsNullOrWhiteSpace(fax))
                            && (item.maximo_Credito == maxcred || maxcred == 0)
                            && (item.comentario.Contains(comentario) || string.IsNullOrEmpty(comentario) || string.IsNullOrWhiteSpace(comentario))
                            && (item.activo == activo || activo)
                        )
                    {
                        dgvClientes.Rows.Add(
                        item.cliente_id
                        , item.Foto
                        , item.primer_nombre
                        + " " + item.segundo_nombre
                        + " " + item.primer_apellido
                        + " " + item.segundo_apellido
                        , item.Cedula
                        , item.activo
                        , item.ruc
                        , item.nombre_factura
                        , item.direccion1
                        , item.telefono1
                        , item.e_mail1
                        , item.fax
                        , item.maximo_Credito
                        , item.comentario
                        , item.direccion2
                        , item.direccion3
                        , item.telefono2
                        , item.e_mail2
                        , item.pais
                        , item.departamento
                        , item.ciudad
                        );

                        // Indicamos que los registros mayores al primero son de lectura
                        dgvClientes.Rows[dgvClientes.Rows.Count - 1].ReadOnly = true;

                    }

                }
                // Al count le restamos -1 para que no tome el cuenta el registro usado para filtrar el grid
                lbl_totalRegistro.Text = "Total Registros: " + (dgvClientes.Rows.Count - 1).ToString();
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void frmCliente_Load(object sender, EventArgs e)
        {            
            RefrescarDatos_del_Servidor();           
        }
        private void btnCerrar_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRefrescar_Click(object sender, EventArgs e)
        {
            CargarClientes();
            
        }

        private void CargarClientes() {
            bgwGetAllClientes.RunWorkerAsync();
        }
        

        private void btnOcultarPanel_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarPanelFiltro(anchoPanelFiltro,panel1);
        }

        private void dgvClientes_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnCerrar_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void btnOcultarPanel_Click_1(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarPanelFiltro(anchoPanelFiltro, panel1);
        }

        private void btnOcultarPanel_Click_2(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarPanelFiltro(anchoPanelFiltro, panel1);
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarPanelFiltro(anchoPanelFiltro, panel1);
        }

        private void btnRefrescar_Click_1(object sender, EventArgs e)
        {
            RefrescarDatos_del_Servidor();
        }

        private void RefrescarDatos_del_Servidor() {
            CargarClientes();
            //CargarTarjetas();
        }

        private ClienteData ObtenerClienteSeleccionado() {
            ClienteData cliente = new ClienteData();
            try
            {
                if (dgvClientes.CurrentRow != null) {
                    var id = Convert.ToInt32(dgvClientes.CurrentRow.Cells["cliente_id"].Value);
                    cliente.cliente_id = id;
                    cliente = _controller.GetAllCliente(id).Data[0];                    
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
            return cliente;
        }

        private void dgvClientes_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            ObtenerClienteSeleccionado();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            ModificarCliente();
        }

        private void ModificarCliente() {
            AdministrarCliente("U");
        }

        private void AdministrarCliente(string operacion) {
            try
            {
                var c = ObtenerClienteSeleccionado();
                if (c != null) {
                    new frmAdminCliente(operacion, c).ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            AgregarCliente();
        }

        private void AgregarCliente() {
            try
            {
                new frmAdminCliente("I", null).ShowDialog(); ;
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void btnConsultar_Click(object sender, EventArgs e)
        {
            ConsultarCliente();
        }

        private void ConsultarCliente() {
            AdministrarCliente("S");
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            EliminarCliente();
        }

        private void EliminarCliente() {
            try
            {
                if (MetodosGenericos.MensajeSINO("¿Está seguro de eliminar el cliente ?") == DialogResult.Yes) {
                    var cliente = ObtenerClienteSeleccionado();
                    var resultado = _controller.RemoveCliente(cliente);

                    MetodosGenericos.MostrarMensaje_RespuestaServidor_GestionCrud(resultado);
                }
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void flowLayoutPanel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            LimpiarFiltros();
        }

        private void LimpiarFiltros() {
            // Informacion datos personales
            txtPrimerNombre.Text = string.Empty;
            txtSegundoNombre.Text = string.Empty;
            txtPrimerApellido.Text = string.Empty;
            txtSegApellido.Text = string.Empty;
            txtCedula.Text = string.Empty;            
            txtRuc.Text = string.Empty;
            chkActivo.Checked = false;

            // Informaciòn de Contacto
            
            txtTelefono1.Text = string.Empty;
            txtTelefono2.Text = string.Empty;
            txtEmail1.Text = string.Empty;
            

            txtMaximoCredito.Text = 0.ToString();
            
        }

        private void cédulaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Cedula,cédulaToolStripMenuItem);
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Foto, toolStripMenuItem1);
        }

        private void nombreCompletoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(NombreCompleto, nombreCompletoToolStripMenuItem);
        }

        private void direccionPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MetodosGenericos.MostrarOcultarColumna(Direccion1, direccionPrincipalToolStripMenuItem);
        }

        private void correoPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MetodosGenericos.MostrarOcultarColumna(Correo1, correoPrincipalToolStripMenuItem);
        }

        private void telefonoPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MetodosGenericos.MostrarOcultarColumna(Telefono1, telefonoPrincipalToolStripMenuItem);
        }

        private void faxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Fax, faxToolStripMenuItem);
        }

        private void rUCToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Ruc,rUCToolStripMenuItem);
        }

        private void máximoCréditoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(MaximoCredito,máximoCréditoToolStripMenuItem);
        }

        private void comentaroiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Comentario,comentaroiToolStripMenuItem);
        }

        private void agregarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AgregarCliente();
        }

        private void modificartoolStripMenuItem_Click(object sender, EventArgs e)
        {
            ModificarCliente();
        }

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ConsultarCliente();
        }

        private void eliminarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EliminarCliente();
        }

        private void dirección2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MetodosGenericos.MostrarOcultarColumna(Direccion2, dirección2ToolStripMenuItem);
        }

        private void dirección3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MetodosGenericos.MostrarOcultarColumna(Direccion3, dirección3ToolStripMenuItem);
        }

        private void télefonoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MetodosGenericos.MostrarOcultarColumna(Telefono1,télefonoToolStripMenuItem);
        }

        private void correo2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //MetodosGenericos.MostrarOcultarColumna(Correo2, correo2ToolStripMenuItem);
        }

        private void paísToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Pais, paísToolStripMenuItem);
        }

        private void departamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Departamento, departamentoToolStripMenuItem );
        }

        private void ciudadToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Ciudad,ciudadToolStripMenuItem);
        }

        private void restaurarColumnasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.RestaurarColumnas(contextMenuStrip1, dgvClientes);
        }

        private void nombreEnFacturaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(NombreFactura,nombreEnFacturaToolStripMenuItem);
        }

        private void activoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Activo,activoToolStripMenuItem);
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            try
            {
                //VisualizarCardsPanelClientes();
            }
            catch (Exception)
            {

                throw;
            }
        }


        private void bgW_ClientesTarjetas_DoWork(object sender, DoWorkEventArgs e)
        {
            
        }

        private void bgW_ClientesTarjetas_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            //LlenarTarjetasHilo();
        }

        private void frmCliente_MaximumSizeChanged(object sender, EventArgs e)
        {
            
        }

        private void frmCliente_Resize(object sender, EventArgs e)
        {
            //CargarTarjetas();
        }

        private void backgroundWorker1_DoWork_1(object sender, DoWorkEventArgs e)
        {
            //CargarTarjetas();
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //CargarTarjetas();
        }

        private void dgvClientes_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            FiltrarDataGridView(sender, e);
        }

        private void FiltrarDataGridView(object sender, DataGridViewCellEventArgs e) {
            try
            {
                
                if (e.RowIndex == 0) {
                    if (dgvClientes.CurrentRow != null) {
                        
                        string nombrecompleto = dgvClientes.CurrentRow.Cells["NombreCompleto"].Value != null ? dgvClientes.CurrentRow.Cells["NombreCompleto"].Value.ToString() : string.Empty;
                        string direccion1 = dgvClientes.CurrentRow.Cells["Direccion1"].Value != null ? dgvClientes.CurrentRow.Cells["Direccion1"].Value.ToString() : string.Empty;
                        string direccion2 = dgvClientes.CurrentRow.Cells["Direccion2"].Value != null ? dgvClientes.CurrentRow.Cells["Direccion2"].Value.ToString() : string.Empty;
                        string direccion3 = dgvClientes.CurrentRow.Cells["Direccion3"].Value != null ? dgvClientes.CurrentRow.Cells["Direccion3"].Value.ToString() : string.Empty;
                        string pais = dgvClientes.CurrentRow.Cells["Pais"].Value != null ? dgvClientes.CurrentRow.Cells["Pais"].Value.ToString() : string.Empty;
                        string departamento = dgvClientes.CurrentRow.Cells["Departamento"].Value != null ? dgvClientes.CurrentRow.Cells["Departamento"].Value.ToString() : string.Empty;
                        string ciudad = dgvClientes.CurrentRow.Cells["Ciudad"].Value != null ? dgvClientes.CurrentRow.Cells["Ciudad"].Value.ToString() : string.Empty;                        
                        string correo1 = dgvClientes.CurrentRow.Cells["Correo1"].Value != null ? dgvClientes.CurrentRow.Cells["Correo1"].Value.ToString() : string.Empty;
                        string correo2 = dgvClientes.CurrentRow.Cells["Correo2"].Value != null ? dgvClientes.CurrentRow.Cells["Correo2"].Value.ToString() : string.Empty;
                        string telefono1 = dgvClientes.CurrentRow.Cells["Telefono1"].Value != null ? dgvClientes.CurrentRow.Cells["Telefono1"].Value.ToString() : string.Empty;
                        string telefono2 = dgvClientes.CurrentRow.Cells["Telefono2"].Value != null ? dgvClientes.CurrentRow.Cells["Telefono2"].Value.ToString() : string.Empty;
                        string fax = dgvClientes.CurrentRow.Cells["Fax"].Value != null ? dgvClientes.CurrentRow.Cells["Fax"].Value.ToString() : string.Empty;
                        string nombrefactura = dgvClientes.CurrentRow.Cells["NombreFactura"].Value != null ? dgvClientes.CurrentRow.Cells["NombreFactura"].Value.ToString() : string.Empty;
                        string cedula = dgvClientes.CurrentRow.Cells["Cedula"].Value != null ? dgvClientes.CurrentRow.Cells["Cedula"].Value.ToString() : string.Empty;                        
                        string ruc = dgvClientes.CurrentRow.Cells["Ruc"].Value != null ? dgvClientes.CurrentRow.Cells["Ruc"].Value.ToString() : string.Empty;
                        string comentario = dgvClientes.CurrentRow.Cells["Comentario"].Value != null ? dgvClientes.CurrentRow.Cells["Comentario"].Value.ToString() : string.Empty;
                        string maximoCredito = dgvClientes.CurrentRow.Cells["MaximoCredito"].Value != null ? dgvClientes.CurrentRow.Cells["MaximoCredito"].Value.ToString() : string.Empty;
                        bool activo = dgvClientes.CurrentRow.Cells["Activo"].Value != null ? bool.Parse(dgvClientes.CurrentRow.Cells["Activo"].Value.ToString()): false;

                        LlenarDataGrid(nombrecompleto, pais, departamento, ciudad, direccion1, direccion2,direccion3, correo1,
                        correo2, telefono1, telefono2, fax, ruc, cedula, comentario,maximoCredito, activo);                        
                    }                    
                }                                               
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }

        private void cardsPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void dirección2ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Direccion2, dirección2ToolStripMenuItem1);
        }

        private void direccion3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Direccion3, direccion3ToolStripMenuItem);
        }

        private void dirección1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Direccion1, dirección1ToolStripMenuItem);
        }

        private void teléfonoPrincipalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Telefono1, teléfonoPrincipalToolStripMenuItem);
        }

        private void teléfono2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Telefono2, teléfono2ToolStripMenuItem);
        }

        private void correoPrincipalToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Correo1, correoPrincipalToolStripMenuItem1);
        }

        private void correo2ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            MetodosGenericos.MostrarOcultarColumna(Correo2, correo2ToolStripMenuItem1);
        }

        private void limpiarFiltrosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MetodosGenericos.LimpiarFiltrosGrid(dgvClientes);
        }

        private void dgvClientes_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ModificarCliente();
        }
       
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using serviceLayer.Security;
using serviceLayer.Security.SecurityServiceReference;

namespace UI.Sif2
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();            
        }

        private void btnIngresar_Click(object sender, EventArgs e)
        {
            AutenticarUsuario();
        }

        private void AutenticarUsuario() {
            try
            {
                var resultado = new SecurityClient().AutenticarUsuario(txtUsuario.Text, txtContraseña.Text);
                if (resultado.CodeResponse == 0) {
                    MetodosGenericos.UsuarioConectado = resultado.Data;
                    new frmMDISIF().Show();
                }
                else
                {
                    if (resultado.CodeResponse == 1) {
                        MetodosGenericos.MensajeAdvertencia(resultado.MessageResponse);
                    }
                    else
                    {
                        if (resultado.CodeResponse == 2)
                        {
                            MetodosGenericos.MensajeError(resultado.MessageResponse);
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                MetodosGenericos.MensajeError_Exception(ex);
            }
        }
    }
}

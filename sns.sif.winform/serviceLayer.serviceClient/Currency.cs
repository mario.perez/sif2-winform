﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serviceLayer.serviceClient
{
    public class Currency: CatalogServiceReference.CurrencyData
    {
        public Currency()
        {

        }

        public Currency(CatalogServiceReference.CurrencyData data)
        {
            this.Id = data.Id;
            this.CurrencyNumber = data.CurrencyNumber;
            this.Decimals = data.Decimals;
            this.IsActive = data.IsActive;
            this.Name = data.Name;
            this.Symbol = data.Symbol;
        }

        public override string ToString()
        {
            return string.Concat("(", Symbol, ") ", Name);
        }

        public CatalogServiceReference.CurrencyData ToCurrencyData()
        {
            var result = new CatalogServiceReference.CurrencyData();
            result.Id = Id;
            result.CurrencyNumber = CurrencyNumber;
            result.Decimals = Decimals;
            result.IsActive = IsActive;
            result.Name = Name;
            result.Symbol = Symbol;
            return result;
        }


    }
}

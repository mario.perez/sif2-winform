﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serviceLayer.serviceClient
{
    public class Configuration: CatalogServiceReference.Configs
    {
        public Configuration()
        {

        }

        public Configuration(CatalogServiceReference.Configs data)
        {
            this.Id = data.Id;
            this.Description = data.Description;
            this.Value = data.Value;
        }

        public CatalogServiceReference.Configs ToConfig()
        {
            var result = new CatalogServiceReference.Configs();
            result.Description = this.Description;
            result.Id = this.Id;
            result.Value = this.Value;
            return result;
        }

        public override string ToString()
        {
            return Id;
        }
    }
}

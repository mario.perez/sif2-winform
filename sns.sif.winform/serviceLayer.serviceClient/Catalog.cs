﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serviceLayer.serviceClient
{
    public class Catalog : CatalogServiceReference.Catalogue
    {
        public Catalog()
        {
            
        }
        public Catalog(CatalogServiceReference.Catalogue data)
        {
            this.Id = data.Id;
            this.Value = data.Value;
            this.IsActive = data.IsActive;
        }

        public override string ToString()
        {
            return string.Concat(Id," - ", Value);
        }

        public CatalogServiceReference.Catalogue ToCatalogue()
        {
            var result = new CatalogServiceReference.Catalogue();
            result.Id = this.Id;
            result.Value = this.Value;
            result.IsActive = this.IsActive;
            return result;
        }

    }
}

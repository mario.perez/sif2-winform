﻿namespace serviceLayer.serviceClient
{
    public class Product: CatalogServiceReference.ProductData
    {
        public Product() { }

        public Product(CatalogServiceReference.ProductData data)
        {
            this.Category = data.Category;
            this.Description = data.Description;
            this.HasStock = data.HasStock;
            this.IsActive = data.IsActive;
            this.Measurement = data.Measurement;
            this.ProductId = data.ProductId;
           // this.Provider = data.Provider;
            this.UnitsPerPackage = data.UnitsPerPackage;
        }

        public override string ToString()
        {
            return string.Concat(this.ProductId," - ",this.Description);
        }

        public CatalogServiceReference.ProductData ToProductData()
        {
            CatalogServiceReference.ProductData result = new CatalogServiceReference.ProductData
            {
                Category = this.Category,
                Description = this.Description,
                HasStock = this.HasStock,
                IsActive = this.IsActive,
                Measurement = this.Measurement,
                ProductId = this.ProductId,
                //result.Provider = this.Provider;
                UnitsPerPackage = this.UnitsPerPackage
            };
            return result;

        }
    }
}

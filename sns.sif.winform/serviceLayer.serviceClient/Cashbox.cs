﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serviceLayer.serviceClient
{
    public class Cashbox: CatalogServiceReference.CashBoxData
    {
        public Cashbox()
        {

        }
        public Cashbox(CatalogServiceReference.CashBoxData data)
        {
            this.Id = data.Id;
            this.Name = data.Name;
            this.IsActive = data.IsActive;
            this.Warehouse = data.Warehouse;
        }

        public override string ToString()
        {
            return string.Concat(Id, " - ", Name,"(",Warehouse.Value,")");
        }
    }
}

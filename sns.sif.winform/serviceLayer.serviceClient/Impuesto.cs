﻿
using System.Text;
using System.Threading.Tasks;

namespace serviceLayer.serviceClient
{
    public class Impuesto : CatalogServiceReference.ImpuestoData
    {
        public Impuesto() { }
        public Impuesto(CatalogServiceReference.ImpuestoData data) {
            this.ImpuestoId = data.ImpuestoId;
            this.Codigo = data.Codigo;
            this.Nombre = data.Nombre;
            this.DescripcionImpuesto = data.DescripcionImpuesto;
            this.Activo = data.Activo;
            this.Predeterminado = data.Predeterminado;
            this.UserCreacion = data.UserCreacion;
            this.UserModificacion = data.UserModificacion;
            this.FechaCreacion = data.FechaCreacion;
            this.FechaModificacion = data.FechaModificacion;
            this.MaqCreacion = data.MaqCreacion;
            this.MaqModificacion = data.MaqModificacion;
            this.AppCreacion = data.AppCreacion;
            this.AppModificacion = data.AppModificacion;
             
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serviceLayer.serviceClient
{
    public class Cliente : CatalogServiceReference.ClienteData
    {
        public Cliente(CatalogServiceReference.ClienteData data) {
            this.cliente_id = data.cliente_id
                ; this.codigo_clte = data.codigo_clte
                ; this.primer_apellido = data.primer_apellido
                ; this.segundo_apellido = data.segundo_apellido
                ; this.primer_nombre = data.primer_nombre
                ; this.segundo_nombre = data.segundo_nombre
                ; this.nombre_factura = data.nombre_factura
                ; this.direccion1 = data.direccion1
                ; this.direccion2 = data.direccion2
                ; this.direccion3 = data.direccion3
                ; this.ciudad = data.ciudad
                ; this.departamento = data.departamento
                ; this.pais = data.pais
                ; this.telefono1 = data.telefono1
                ; this.telefono2 = data.telefono2
                ; this.fax = data.fax
                ; this.Contacto = data.Contacto
                ; this.e_mail1 = data.e_mail1
                ; this.e_mail2 = data.e_mail2
                ; this.activo = data.activo
                ; this.ruc = data.ruc
                ; this.maximo_Credito = data.maximo_Credito
                ; this.comentario = data.comentario
                ; this.UserCreacion = data.UserCreacion
                ; this.UserModificacion = data.UserModificacion
                ; this.FechaCreacion = data.FechaCreacion
                ; this.FechaModificacion = data.FechaModificacion
                ; this.AppCreacion = data.AppCreacion
                ; this.AppModificacion = data.AppModificacion
                ; this.MaqCreacion = data.MaqCreacion
                ; this.MaqModificacion = data.MaqModificacion;
                ; this.Cedula = data.Cedula
                ; this.Foto = data.Foto;
        }
    }
}

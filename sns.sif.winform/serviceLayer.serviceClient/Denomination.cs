﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serviceLayer.serviceClient
{
    public class Denomination: CatalogServiceReference.DenominationData
    {
        public Denomination()
        {

        }

        public Denomination(CatalogServiceReference.DenominationData data)
        {
            this.Id = data.Id;
            this.Description = data.Description;
            this.CurrencyIso = data.CurrencyIso;
            this.Factor = data.Factor;
            this.IsActive = data.IsActive;
            this.IsCash = data.IsCash;
        }

        public override string ToString()
        {
            return string.Concat(CurrencyIso.Symbol, " - ", Description);
        }
    }
}

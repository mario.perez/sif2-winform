﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace serviceLayer.serviceClient
{
    public class Provider: CatalogServiceReference.ProviderData
    {
        public Provider()
        { }

        public Provider(CatalogServiceReference.ProviderData data)
        {
            this.ProviderId = data.ProviderId;
            this.ProviderName = data.ProviderName;
            this.IsActive = data.IsActive;
            this.Phone = data.Phone;
            this.Address = data.Address;
            this.ProviderCodeId = data.ProviderCodeId;
            this.Email = data.Email;
            this.Contact = data.Contact;
            
        }

        public override string ToString()
        {
            return string.Concat(ProviderName);
        }
    }
}
